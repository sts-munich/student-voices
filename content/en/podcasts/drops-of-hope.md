---
date: 2017-04-10T11:00:59-04:00
description: "Making Water in Puebla"
featured_image: "/images/drops-of-hope.jpg"
tags: [podcast]
title: "Drops of Hope: Making Water in Puebla"
cover_dimming_class: bg-black-10
---

What is good innovation? What kind of innovation is it when it comes from outside the Silicon Valley elite? To find innovation outside the walls of Northern California, we changed how we define innovation, who makes it, and where to look for it. That’s how we ended up in Puebla, Mexico.

Puebla is a region in the southeastern part of Mexico, 300 kilometers south of the modern, resource-rich capital Mexico City. In Puebla, their principal concern was not renewable energy or upgrading the power grid, but rather, where and how to access enough water to live.  In 1988, after eight years working with the communities of Puebla, listening to them and building trust, Gisela and Raul Herrerías’ made the decision together with the community of Santa María la Alta to build a dam. Their organization, Agua Para Siempre, secured a financial agreement with the local government but, right before they were about to start construction, the government backed out of the deal.  That was the first of many hope-crushing challenges they faced together, as they tried to bring a much-needed innovation for a much-needed resource.

This podcast is about innovation, but features no app, no venture capitalist, and makes no promises to “save the world”. This podcast is about building trust, about  resilience, dedication, and of a community working together to create a better world for the next generations. This is Drops of Hope: Making Water in Puebla.

*Visit our website at www.drops-of-hope.com where you’ll see how the project “Agua para Siempre” evolved, the transcript of the episode and more info on our interviewees. This podcast was produced by Paola Segovia, Jenny Graner, Camila Hidalgo, Michael Makled and Matthias Meller.*

### Our interviewees

- Gisela Herrerías, Co-Founder of Agua para Siempre
- Sebastian Pfotenhauer, Assistant Professor for Innovation Research at Technical University of Munich
- Carlos Cuevas, Postdoctoral Researcher at the Innovation, Society and Public Policy Research Group at the Munich Center for Technology in Society
- Yolanda López, Independent Researcher, Human ecologist and Geographer specialized in freshwater resources

### Further reading

- Pfotenhauer, S., & Jasanoff, S. (2017). Panacea or diagnosis? Imaginaries of innovation and the ‘MIT model’ in three political cultures. Social Studies of Science, 47(6), 783–810. https://doi.org/10.1177/0306312717706110
- Godin, B. (2015). Innovation Contested: The Idea of Innovation Over the Centuries (1st ed.). Routledge. https://doi.org/10.4324/9781315855608
- Jan Fagerberg, David C. Mowery, Bjørn T. Asheim, & Meric S. Gertler. (2009). The Geography of Innovation: Regional Innovation Systems. https://doi.org/10.1093/oxfordhb/9780199286805.003.0011
- Kuhlmann, S., & Ordóñez-Matamoros, G. (Eds.). (2017). Research handbook on innovation governance for emerging economies: Towards better models. Edward Elgar Publishing Ltd.
- Wisnioski, M., Hintz, E. S., & Kleine, M. S. (Eds.). (2019). Does America Need More Innovators? The MIT Press. https://doi.org/10.7551/mitpress/11344.001.0001
