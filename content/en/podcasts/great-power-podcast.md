---
date: 2017-04-09T10:58:08-04:00
description: "Invisibility"
featured_image: ""
tags: ["scene"]
title: "Great Power Podcast: Invisibility"
---

Hollywood is filled with movies about heroes and villains with powers beyond the grasp of humanity, but what if those powers were within our reach? Science and technology continue to push the boundaries of what we consider possible, and many of these achievements resemble the abilities portrayed in the fantasy, science fiction, and superhero genres. The Great Power Podcast explores two sides of super-powered innovation: the scientific and technological progress that bring these powers to life and the societal implications that come along with their development.

For this episode, we set our sights on the power of Invisibility. We wanted to know how close we are to truly becoming invisible and what impact that might have on the world as we know it. To learn more about this, we turned to the experts:

- Guy Cramer: Inventor of patent pending Quantum Stealth (Invisibility Cloak). Keynote Speaker. President/CEO of Hyperstealth Biotechnology Corp.
- William Jones: Founder of Afrofuturism Network, is a historian, “comic book geek,” writer, and educator. Expert regarding the image of black people in various forms of media, pop culture and hip-hop music.
- Alon Gorodetsky: Associate Professor, Department of Chemical Engineering and Materials Science, University of California, Irvine
- David Ryan Polgar (Not featured): Tech Ethicist & Digital Citizenship Expert, Founder of All Tech Is Human, Member of TikTok’s Content Advisory Council
- Robert Sharp: Chair of Religion and Philosophy Departments, Associate Professor of Philosophy. Author of Above the Social Contract? How Superheroes Break Society.

