---
title: Podcasts
description: ""
type: page
---

Podcasts today are omnipresent. For many, they have become the preferred, on-demand radio for a morning or afternoon commute. They are everywhere and for good reasons – listening to ideas in the form of interviews and stories can be an effective way to learn and understand a new topic. 

For the first time at the MCTS, Professor Ruth Mueller’s seminar “Telling Responsible Stories – Telling Stories Responsibly” drove students to understand the ins and outs of storytelling practices from an STS perspective. By studying storytelling practices, students dove deeper into understanding how sustainable and responsible solutions are presented and constructed in contemporary society. The study of these grand narratives, inspired by work from STS scholars such as Donna Haraway, challenges how stories of solutions can be political, economic, or technological in nature.  In the seminar, students were encouraged to rethink practices of storytelling and to tell the “story otherwise” in the narrative genre of podcasts. As the final project for the seminar, students produced their own podcasts, which are available to you here at Student Voices – tune in.
