---
# title: "Ananke: a Hugo Theme"
omit_header_text: true
#description: "The last theme you'll ever need. Maybe."
---

# Welcome

You want to know more about student life at the MCTS? Great, then you have come to the right place! Student Voices is a place where students’ creative outbursts such as essays, blog articles, or even podcasts are gathered and shared with the rest world. You can also learn more about our various student working groups such as MCTS Create and, well, Student Voices. We also keep you posted about all the big events and happenings organized by our students such as the Student Spring Gathering and the Science School. So, dive in and find out what student life at the MCTS is all about.