---
date: 2019-11-10T11:00:59-04:00
description: "Differentiation of the science and the responsibility of innovation"
featured_image: "/images/science-in-hd-qmLwjOEDFrE-unsplash.jpg"
tags: [article]
title: "Directed Mutagenesis in the EU"
author: "Annika Essmann"
---

### Synopsis

This article, by Annika Essmann of the M.A. RESET program, reviews the European Court of Justice’s decision on GMOs. It analyzes the ascription of responsibility and suggests an alternative framing for it. The paper was originally written as an assignment for the law module of the Technology and Society course.

The Conseil d’État in France (The Council of State) (the Conseil) appealed to the European Court of Justice (ECJ). It asked the ECJ whether organisms obtained by mutagenesis constitute GMOs and which of its methods shall be excluded from a Directive 2001/18/EC (the Directive). This Directive aims to protect human health and the environment regarding the release of GMOs (ECJ, 2018a, Art.4). France largely harmonised their law with the Directive (ECJ, 2018a, Art.15-19). Generally, mutagenesis allows one to alter the genome of a living species without the insertion of foreign DNA. New mutagenesis techniques allow to produce organisms which are, for instance, resistant to certain herbicides (ECJ, 2018b). These new methods are called directed mutagenesis (ECJ, 2018a, Art.23). The ECJ ruled that all organisms obtained by mutagenesis are GMOs (ECJ, 2018a, Art.30). How is responsibility in this judgement framed and distributed?

***

#### Context of the decision

The Conseil d’État (the Conseil) appealed to the ECJ during proceedings in which the Conseil was specifically asked to rule on the methods of directed mutagenesis (ECJ, 2018a, Art.47). Generally, mutagenesis allows one to alter the genome of a living species without the insertion of foreign DNA. New mutagenesis techniques allow to produce organisms which are, for instance, resistant to certain herbicides (ECJ, 2018b). These new methods are called directed mutagenesis (ECJ, 2018a, Art.23).

The parties involved in the proceedings were a French agricultural union and eight associations (the associations) (ECJ, 2018b), and two French ministers (ECJ, 2018a, Art.2). The associations contested the French legislation which was harmonised with the Directive. French law exempts organisms obtained by mutagenesis from the obligations laid down by the Directive. They thought that organisms obtained by mutagenesis, such as herbicide-resistant varieties, carry the same risk as GMOs (ECJ, 2018b). Consequently, they demanded (1) to include mutagenesis as a technique which results in genetic modification, (2) to ban the cultivation and marketing of herbicide-resistant varieties obtained by mutagenesis and (3) to order the Ministers to introduce a moratorium on herbicide-resistant varieties (ECJ, 2018a, Art.20).

To understand these statements better, the Directive and French law are briefly summarised.

#### Directive 2001/18 and French law

The Directive aims to protect human health and the environment regarding the release of GMOs (ECJ, 2018a, Art.4). Under the Directive, GMOs are organisms whose genetic material has been altered in an unnatural way (ECJ, 2018a, Art.5). When an organism falls under this definition, EU Member States should act according to the precautionary principle (PP) (ECJ, 2018a, Art.7). Essentially, the PP is at the heart of the Directive (ECJ, 2018a, Art.4).

The Directive neither applies to organisms obtained through techniques which have conventionally been used or which have a long safety record (ECJ, 2018a, Art.3) nor to techniques listed in Annex I B, mutagenesis is one of them (ECJ, 2018a, Art.10). That implies organisms obtained through mutagenesis are not considered GMOs.

France largely harmonised their law with the Directive (ECJ, 2018a, Art.15-19). For instance, the definition of GMOs is comparable (ECJ, 2018a, Art.15).

#### Interpretation of Directive 2001/18

Before this legal background, the Conseil asked the ECJ whether organisms obtained by mutagenesis constitute GMOs and which of its methods shall be excluded from the Directive’s obligations (ECJ, 2018a, Art.25,1).

The ECJ ruled that all organisms obtained by mutagenesis are GMOs (ECJ, 2018a, Art.30). Their argumentation was two-fold. First, the risks of directed mutagenesis are similar to those of transgenesis which is listed as a technique resulting in GMO (ECJ, 2018a, Art.48). Second, if mutagenesis was excluded from the scope of the Directive, it would fail to meet the intention of the Directive of protection (ECJ, 2018a, Art.51) and it would further disrespect the PP which the Directive seeks to implement (ECJ, 2018a, Art.53). As such, the ECJ viewed mutagenesis as resulting into GMOs and only conventional mutagenesis are excluded (ECJ, 2018a, Art.54).

### Framing and distribution of responsibility in the ECJ judgement

#### Description of the precautionary principle

The concept framing the judgement, indeed the entire Directive, is the precautionary principle (PP). Its basic premise is that governments should act to protect human health and the environment, regardless of the costs, when there is a threat of serious damage, although scientific evidence of this harm might be lacking. Consequently, this principle imposes a substantive duty of care upon governments (Victor, 2001, p.315), yet, it is meant to help politicians to make sound public policy decisions in the face of scientific uncertainty (Harremoës et al., 2001, p.15).

It is noteworthy that the PP is different to the prevention principle. The PP deals with uncertain risks, the prevention principle deals with known risks. Furthermore, the PP obliges states not to defer regulatory action despite missing scientific evidence, the prevention principle obliges states to take action at the earliest stage possible (Van Calster, 2008, p.66). That implies that the PP is proactive in nature, action is taken before a dangerous situation occurs, but the prevention principle is reactive, action is taken after a danger has been identified (Tait, 2001, p.2). Interestingly, the application of the PP was partly an attempt to improve the ‘reactive’ prevention principle, and, even more interestingly, the European industry producing GMOs was the first to be regulated by the PP (Tait, 2001, p.3).

When applying the PP, the main responsibility lies with policy makers and researchers producing GMOs to take precautious measures and to provide a proof of harm (safety), respectively. In case of the ECJ judgement this means that Members also have to regulate directed mutagenesis techniques now and every institution working with these techniques has to abide by the Directive’s obligations. On the one hand, that can increase safety for society en large, but it also burdens national policy makers and researchers further. In short, the ECJ decision led to a shift of responsibilities towards policy and research.

Apart from these specific consequences of the PP, there are also general objections to the PP as a regulatory tool which are discussed next.

### Criticism on the precautionary principle

Some critics oppose the PP because it interferes with principles of free trade since it enables a nation to restrict the import of a product based on its subjective perception that it poses risks (Victor, 2001, p.319-320). Indeed, the EU came into conflict regarding that issue. Its precautionary approach to GMOs resulted in a dispute before the World Trade Organisation in 2006 (Peel, 2010, p.135-137) in which the EU was accused of trade protectionism (Morris & Spillane, 2008, p.500).

Other critics discuss that the Directive will become an insufficient regulatory tool because the Directive will struggle to consider similar risks posed by non-GM-based approaches, such as targeted mutagenesis. To them, it is problematic that the Directive’s current process of GM safety evaluation is not well balanced with ‘conventionally’ bred plants, partially because conventional methods are simply classified as such due to familiarity rather than scientific understanding (Morris & Spillane, 2008, p.502). In short, the Directive lacks a scientific foundation. This claim of inadequate distinction is especially interesting as it exemplifies what happened in the ECJ case; the Directive was unable to make a distinction, so the ECJ had to make one. This is remarkable because these worries were put forward in 2008, almost 10 years before the court judgement.

Considering these oppositions, an alternative framing of the Directive and hence, the court judgement is proposed subsequently.

### Alternative framing and distribution of responsibility

The PP as articulated in the Directive focuses on the end of a development process since it applies the PP to the release of a GMO, not its research. A different perspective further upstream might be more helpful.

The first concept that springs to mind in this case could be ‘safer by design’. This approach aims to integrate knowledge of adverse effects into the design process of a new technology and it is intended to engineer these effects ‘out’ (Schwarz-Plaschg, Kallhoff, & Eisenberger, 2017, p.277). However, as promising as this might sound, the focus on safety could marginalise other issues, such as the societal desirability of innovation. Yet, it is important to explore other values to be included into an upstream evaluation (Schwarz-Plaschg et al., 2017, p.278).

Instead, the concept of responsible innovation (RI) appears more suitable. Owen et al (2013) define it as a “collective commitment of care for the future through responsive stewardship of science and innovation in the present” (p.36). According to them, it is one major task of RI to determine which futures we want science and innovation to create. This consideration, they argue, poses the challenges of accommodating a plurality of political and ethical views as an input and, then, prioritising the different futures (Owen et al., 2013, p.37). To finally achieve RI, four dimensions should be integrated, namely:

- Anticipation: describing and analysing (un)intended impacts through foresight
- Reflection: reflecting on underlying purposes, uncertainties and risk
- Deliberation: listening to perspectives from publics and diverse stakeholders
- Responsiveness: setting the direction of innovation through reflexivity and learning (Owen et al., 2013, p.37-38)

This approach appears more useful because it not only considers anticipation, which is characteristic for the PP, but deliberation provides input for this anticipation and reflection as well as responsiveness hold anticipation in check. For instance, a GMO regulation, like the Directive, would not just accept precaution for human (environmental) hazards as a regulatory measure whose perception can be manipulated by pressure groups benefiting from a tighter regulation (Morris & Spillane, 2008, p.503). Instead, it would additionally consider other viewpoints and reflect upon the underlying purposes of those holding these views.

However, this model has a deficit which Ruggiu (2015) outlines. He states that the socio-empirical version of RI – Owen et al.’s model can arguably be considered as one version – is problematic in two ways. First, participation (deliberation) does not necessarily lead to more democratic or better outcomes because only particular stakeholders in a particular mode of engagement can participate (Ruggiu, 2015, p.227). Second, the relationship between future visions, one main tasks of RI according to Owen et al. (2013, p.37), and regulation is difficult because one would still use current regulatory frameworks for the development of unforeseen processes; this, in turn, confirms inadequate regulations (Ruggiu, 2015, p.228).

To avoid these problems, Ruggiu argues that human rights should be primarily considered, as already legitimised principles guiding RI so to speak (Ruggiu, 2015, p.229). Specifically, human rights belong to the person, while fundamental rights belong to a citizen (Ruggiu, 2015, p.230). This inclusion of human rights is important because, first, the socio-empirical version of RI does not necessarily produce respect for fundamental rights (Ruggiu, 2015, p.231). Second, nothing in EU law leads to the priorisation of fundamental rights, meaning that in cases concerning innovations, such as directed mutagenesis, fundamental rights might be placed after the promotion of public interests, such as techno-scientific progress (Ruggiu, 2015, p.230). So instead, human rights should be included in the considerations of RI because they can ‘override’ public interests.

In sum, the alternative framing for the Directive proposed here is the RI model by Owen et al. (2013) complemented by human rights as principles guiding the deliberation and reflection process.

### Personal opinion

I generally support the decision of the ECJ. First, I am convinced that one should always consider scientific development and make distinctions, such as between random and directed mutagenesis. I think it is paramount to promote consistency, especially in policy, to treat similarly risky endeavours equally. Second, I think that when considering little details, for instance specific GMO-techniques, one should not forget the larger purpose. Indeed, I found it remarkable that the Directive’s objective to protect humans and the environment ‘overrode the Annexes’ and I am convinced that this is how it should be.

Despite my general agreement, I also have two points of critique. First, I find the definition of the conventional GMO-methods, exempted from the Directive’s obligations, especially vague. What is convention and according to whom? I think this could be formulated more concretely to avoid that techniques pass as conventional although they are hazardous. Second, I think the role of experts is too prominent. Certainly, I value scientific progress, as mentioned above, but the policy process seems to be largely based on their expertise. This gives experts enormous power although their knowledge is far from objective (Jasanoff, 2003, p.160).

In summary, I support the alternative framing and I would complement this view by sharpening the definition of conventional GMO-methods and monitoring the scientific influence.

***

### References

ECJ. (2018a). Confédération paysanne and Others v Premier ministre and Ministre de l’Agriculture, de l’Agroalimentaire et de la Forêt (C‐528/16). Retrieved from http://curia.europa.eu/juris/document/document_print.jsf?docid=204387&text=&dir=&doc lang=EN&part=1&occ=first&mode=req&pageIndex=0&cid=2927832#Footnote*

ECJ. (2018b). Organisms obtained by mutagenesis are GMOs and are, in principle, subject to the obligations laid down by the GMO Directive [Press release]. Retrieved from https://curia.europa.eu/jcms/jcms/Jo2_7052/en/?annee=2018

Harremoës, P., Gee, D., MacGarvin, M., Stirling, A., Keys, J., Wynne, B., & Vaz, S. G. (2001). Late lessons from early warnings: the precautionary principle 1896-2000: European Environment Agency.

Jasanoff, S. (2003). (No?) Accounting for expertise. Science and Public Policy, 30(3), 157-162.

Morris, S. H., & Spillane, C. (2008). GM directive deficiencies in the European Union: the current framework for regulating GM crops in the EU weakens the precautionary principle as a policy tool. EMBO Reports, 9(6), 500-504.

Owen, R., Stilgoe, J., Macnaghten, P., Gorman, M., Fisher, E., & Guston, D. (2013). A framework for responsible innovation. In R. Owen, J. Stilgoe, P. Macnaghten, M. Gorman, E. Fisher, & D. Guston (Eds.), Responsible Innovation: Managing the Responsible Emergence of Science and Innovation in Society (pp. 27-50). Chichester, West Sussex: Wiley & Sons.

Peel, J. (2010). Science and risk regulation in international law. Cambridge University Press.

Ruggiu, D. (2015). Anchoring European governance: two versions of responsible research and innovation and EU fundamental rights as ‘normative anchor points’. NanoEthics, 9, 217- 235.

Schwarz-Plaschg, C., Kallhoff, A., & Eisenberger, I. (2017). Making Nanomaterials Safer by Design? NanoEthics, 11, 277-281.

Tait, J. (2001). More Faust than Frankenstein: the European debate about the precautionary principle and risk regulation for genetically modified crops. Journal of Risk Research, 4(2), 175-189.

Van Calster, G. (2008). Risk regulation, EU law and emerging technologies: Smother or smooth? NanoEthics, 2(1), 61-71.

Victor, M. (2001). Precaution or Protectionism–The Precautionary Principle, Genetically Modified Organsims, and Allowing Unfounded Fear to Undermine Free Trade. Transnational Law, 14, 295. Available at: https://scholarlycommons.pacific.edu/globe/vol14/iss1/14
